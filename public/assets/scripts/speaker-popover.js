$(document).ready(function() {
    $('#mason-div').popover({
      placement: 'bottom',
      title: 'Christopher E. Mason (he/him)',
      content: 'Christopher Mason, PhD is a Professor of Genomics, Physiology, Biophysics, and Neuroscience at Weill Cornell Medicine and Director of the WorldQuant Initiative for Quantitative Prediction. He completed a dual B.S. in Genetics & Biochemistry at University of Wisconsin-Madison (2001), a Ph.D. in Genetics at Yale University (2006), Clinical Genetics Fellowship at Yale Medical School (2009), and was the Visiting Fellow of Genomics, Ethics, and Law at Yale Law School (2006-2009). His laboratory creates and deploys new technologies and algorithms for medicine, integrative omics, and cell/genome engineering, spanning >350 peer-reviewed papers, five patents, five diagnostics tests, 10 biotechnology companies, and 4 non-profits. Dr. Mason also holds affiliate faculty appointments at the New York Genome Center, Yale Law School, and the Consortium for Space Genetics at Harvard Medical School. He is the author of The Next 500 Years: Engineering Life to Reach New Worlds and The Age of Prediction.',
      trigger: 'click',
      template: '<div class="popover speaker-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
    });

    // Hide popover when clicking outside or on another popover
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        // hide any open popovers when anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    // popover z-index fix under the navbar
    $('.popover').css('z-index', '100');
    });
});
